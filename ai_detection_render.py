from module.decision_tree.igo_decision_tree import DecisionTree

dectree = DecisionTree()
dectree.fit_with_jsonfile( "preprocessed_dataset.txt",10)
dectree.render()
