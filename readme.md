# Note

  

현재 이 솔루션은 nginx의 web access log만 파싱가능 합니다.

* Nginx access log 포멧

```
'$remote_addr - $remote_user [$time_local] "$request" $status $body_bytes_sent "$http_referer" "$http_user_agent" "$http_x_forwarded_for"';
```

  


# Usage


  ## GUI Application

  ```
  python ai_detector.py
  ```

사용법 참고 자료 링크 : https://www.youtube.com/watch?v=EoHYzP3CAj8







## CUI Application

* 결정트리 도식도 렌더링

```bash
python ai_detection_main.py

```

* 판별

  

```bash
python ai_detection_main.py test.txt

```

  test.txt에는 분석하고자하는 웹접근로그 레코드 한개가 저장된 파일.



사용법 참고 자료 링크 : https://www.youtube.com/watch?v=FQWqeNHn4zI



