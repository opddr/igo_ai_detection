import sys
import os
import queue
import json
import re
import time
import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from keras.preprocessing.sequence import pad_sequences
from module.decision_tree.igo_decision_tree import DecisionTree
from PyQt5.QtWidgets import *
from PyQt5.QtGui import QMovie
from PyQt5 import QtCore, QtGui, QtWidgets
from ast import literal_eval
import itertools

sequence_maxlength = 80

charset = ("a","b","c","d","e","f","g","h","i","j","k","l","m","n","o",
            "p","q","r","s","t","u","v","w","x","y","z","`","~","1","!",
            "2","@","3","#","4","$","5","%","6","^","7","&","8","*","9",
            "(","0",")","-","_","=","+","[","{","]","}","\\","|",";",":",
            "\'","\"",",","<",".",">","/","?","※","☆","♡","\b","\t","\n","\c",
            "\r"," "
)

config_ip_certfd = open("config/ip_cert.txt","r")
config_ip_devfd = open("config/ip_dev.txt","r")
config_ip_domesticfd = open("config/ip_domestic.txt","r")
config_ip_foreignfd = open("config/ip_foreign.txt","r")
config_ip_intrafd = open("config/ip_intra.txt","r")
config_ip_suspiciousfd = open("config/ip_suspicious.txt","r")
config_port_certfd = open("config/port_cert.txt","r")
config_port_common_usefd = open("config/port_common_use.txt","r")
config_port_devfd = open("config/port_dev.txt","r")
config_port_exploitablefd = open("config/port_exploitable.txt","r")
config_port_issuefd = open("config/port_issue.txt","r")

ip_cert = config_ip_certfd.read().splitlines()
ip_dev = config_ip_devfd.read().splitlines()
ip_domestic = config_ip_domesticfd.read().splitlines()
ip_foreign = config_ip_foreignfd.read().splitlines()
ip_intra = config_ip_intrafd.read().splitlines()
ip_suspicious = config_ip_suspiciousfd.read().splitlines()
port_cert = config_port_certfd.read().splitlines()
port_common_use = config_port_common_usefd.read().splitlines()
port_dev = config_port_devfd.read().splitlines()
port_exploitable = config_port_exploitablefd.read().splitlines()
port_issue = config_port_issuefd.read().splitlines()





def one_hot_ecoding(str):

    x = []
    for i,char in enumerate(str):
        try:
            num = charset.index(char)
        except:
            print("Unhandeld Character :",char)
            sys.exit()
            
        vector = [0] * len(charset)
        vector[num] = 1
        x.append(vector)
    
    return x


def file_to_hotencoding(path,label_number):

    X = []
    Y = []

    lines = open(path,"r").readlines()
    for line in lines:
        line = line.lower()
        x = one_hot_ecoding(line)
        X.append(np.asarray(x).astype(np.float32))
        Y.append(label_number)
        
    return X,Y
    
    
    
X,Y = file_to_hotencoding("config/pay_load_threat.txt",1)
legX, legY = file_to_hotencoding("config/pay_load_legitimate.txt",0)
X.extend(legX)
Y.extend(legY)
X = pad_sequences(X,dtype='object',maxlen=sequence_maxlength,value= [0]*len(charset),padding="post")
X = np.asarray(X).astype(np.float32)
Y = np.asarray(Y).astype(np.float32)
model = keras.Sequential()
model.add(keras.Input((None,77)))
model.add(layers.LSTM(50))
model.add(layers.Dense(1, activation='sigmoid'))
model.compile(optimizer="Adam", loss="mse")
model.fit(X, Y,epochs=100, batch_size=50, verbose=1)


dectree = DecisionTree()
dectree.fit_with_jsonfile( "preprocessed_dataset.txt",10)


def ip_assessment(ip):
    if ip in ip_cert :
        return -100
    elif ip in ip_dev :
        return -50
    elif ip in ip_intra :
        return -10
    elif ip in ip_domestic :
        return 0
    elif ip in ip_foreign :
        return 20
    elif ip in ip_suspicious :
        return 90
    else:
        return 20
        


    
def port_assessment(port):

    
    if port in port_cert:
        return -100
    elif port in port_dev:
        return -50
    elif port in port_common_use:
        return 0
    elif port in port_exploitable:
        return 10
    elif port in port_issue:
        return 50
    else:
        return 1

        
    
def payload_assessment(payload):
    payloads = []
    payload = payload.lower()
    payload = one_hot_ecoding(payload)
    payloads.append(payload)
    payloads = pad_sequences(payloads,dtype='object',maxlen=sequence_maxlength,value= [0]*len(charset),padding="post")
    predictions = model.predict(np.asarray(payloads).astype(np.float32))
    return int(predictions[0][0]*100)
    


def classifier(log):

    access_log = log
    found = re.findall(r"(.*?) .*? .*? \[.*\] \".*? (.*) (HTTPS?).*?\" \d\d\d \d.*",access_log)

    if found == []:
        print(found)
        raise Exception("Invalid log format.")
        

    if found[0] == [] or found[0] == [] or found[0] == [] :
        print(found)
        raise Exception("Invalid log format.")
       

        
    found = found[0]
    port = 0
    if found[2]=="HTTP":
        port = 80
    else:
        port = 443
    parsed = {"ip_risk":ip_assessment(found[0]),"payload_risk":payload_assessment(found[1]),"port_risk":port_assessment(port)}
    res,chain = dectree.predict(parsed)
    if res == 0:
        return "정상적인 웹 접근입니다.",chain
    elif res == 1:
        return "의심되는 트래픽입니다. 상세분석을 요청합니다.",chain
    elif res == 2:
        return "공격적인 웹 접근입니다.",chain
    




class Window(QMainWindow):
    def __init__(self):
        super().__init__()

        self.initUI()

    def initUI(self):
        
        self.setWindowTitle('AI Detector')
        self.setFixedSize(700,650)
        
        self.hbox = QHBoxLayout()
        self.vbox_information = QVBoxLayout()
        self.vbox_graph = QVBoxLayout()
        
        

        self.label1 = QLabel('Log 입력창',self)
        self.textedit = QTextEdit(self)
        self.label2 = QLabel('분석 결과창',self)
        self.textedit2 = QTextEdit(self)
        self.reason_chain = QTextEdit(self)
        
        self.label1.move(40,0)

        self.textedit.move(40, 30)
        self.textedit2.move(40, 560)
        self.label2.move(40,535)
        self.textedit.resize(250,500)
        self.textedit2.resize(250,30) 
        self.textedit2.setReadOnly(True)
        self.reason_chain.move(120+40+250, 30)
        self.reason_chain.setReadOnly(True)
        self.reason_chain.resize(250,500) 
        
        
        analysis_button = QPushButton("분석시작",self)
        analysis_button.clicked.connect(self.analysis_clicked)
        analysis_button.move(60,600)
        rendering_tree_button = QPushButton("결정 트리 보기",self)
        rendering_tree_button.clicked.connect(self.rendering_clicked)
        rendering_tree_button.move(160,600)
        

        self.show()


    def analysis_clicked(self):
        self.textedit2.setText("분석 중 입니다.")
        self.textedit2.repaint()
        
        log = self.textedit.toPlainText()
        try:
            res,chain = classifier(log)
        except Exception as e:
            print(e)
            QMessageBox.about(self, "Alert", "Invalid log format.")
            self.textedit2.setText("")
            return

        self.textedit2.setText(res)
        reason_chain = ""
        for chain_node in chain:
            for key in chain_node.keys():
               reason_chain += key + " : " + str(chain_node[key]) + "\n\n" 
                
        self.reason_chain.setText(reason_chain)
    def rendering_clicked(self):
        #QMessageBox.about(self, "Note","미구현 기능입니다.")
        SubWindow().showModal()

class SubWindow(QDialog):
    def __init__(self):
        super().__init__()
        hbox = QHBoxLayout()
        vbox = QVBoxLayout(self)
        groupBox = QGroupBox()    
        screen = QDesktopWidget().screenGeometry()        

        max_count_features = dectree.max_count_features
        max_count_classes = dectree.max_count_classes
        max_depth = dectree.max_depth
        feature_names = dectree.feature_names

        q = queue.Queue()
        q.put(dectree.dectree)
        prevdepth = 1
        n = None
        while not q.empty():
            
            if n !=None:
                prevdepth = n.depth
            n = q.get()

            if prevdepth < n.depth :                
                vbox.addLayout(hbox)
                hbox = QHBoxLayout()

                

            if n.involvedclass == -1:
                b = QPushButton("feature : "+feature_names[n.feature]+"\ntheshold : "+str(n.threshold)+"\nimpurity : "+str(n.impurity))
                b.setMaximumWidth(300)
                b.setMinimumHeight(100)
                tooltip_string = "left"
                for i,leftss in enumerate(n.leftsubset):
                    tooltip_string += "\n class "+str(i)+":"+str(len(leftss))
            
                tooltip_string += "\nright"
                for i,rightss in enumerate(n.rightsubset):
                    tooltip_string += "\n class "+str(i)+":"+str(len(rightss))
                b.setToolTip(tooltip_string)
                b.setStyleSheet("background-color:rgb("+str(n.color[0])+","+str(n.color[1])+","+str(n.color[2])+");")
                hbox.addWidget(b)



            elif n.involvedclass == -2 :
                string = "Bad leaf !!!"
                i = 0
                for leftss,rightss in itertools.zip_longest(n.leftsubset,n.rightsubset):
                    string += "\n class "+str(i)+":"+str(len(leftss)+len(rightss))
                    i+=1
                b=QPushButton(string)
                b.setMaximumWidth(300)
                b.setMinimumHeight(100)
                b.setStyleSheet("background-color:rgb("+str(n.color[0])+","+str(n.color[1])+","+str(n.color[2])+");")

                tooltip_string = "sample\n"
                for i in range(0,len(feature_names)):
                    tooltip_string += " "+feature_names[i]+" : "+str(n.sample_data[i])+"\n"

                b.setToolTip(tooltip_string)
                hbox.addWidget(b)
                
                
            else :
                b= QPushButton("It is leaf as : ["+str(n.involvedclass)+"] - ")
                b.setMaximumWidth(300)
                b.setMinimumHeight(100)
                b.setStyleSheet("background-color:rgb("+str(n.color[0])+","+str(n.color[1])+","+str(n.color[2])+");")

                i = 0
                tooltip_string = "leaf"
                for leftss,rightss in itertools.zip_longest(n.leftsubset,n.rightsubset):
                    tooltip_string += "\n class "+str(i)+":"+str(len(leftss)+len(rightss))
                    i+=1
                b.setToolTip(tooltip_string)
                hbox.addWidget(b)

            if n.left != None:
                q.put(n.left)
            if n.right != None:
                q.put(n.right)


        vbox.addLayout(hbox)


        groupBox.setLayout(vbox)
        scroll = QScrollArea(self)
        scroll.setWidgetResizable(False)
        scroll.setWidget(groupBox)


        self.setGeometry(0,0,screen.width(),screen.height())
        scroll.setGeometry(0,0,screen.width()-10,screen.height()-50)
        
    def showModal(self):
            super().exec_()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Window()
    sys.exit(app.exec_())
    
    





    