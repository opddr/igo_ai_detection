import sys
import os
import queue
import json
import re
import time
import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from keras.preprocessing.sequence import pad_sequences
from module.decision_tree.igo_decision_tree import DecisionTree

sequence_maxlength = 80

charset = ("a","b","c","d","e","f","g","h","i","j","k","l","m","n","o",
            "p","q","r","s","t","u","v","w","x","y","z","`","~","1","!",
            "2","@","3","#","4","$","5","%","6","^","7","&","8","*","9",
            "(","0",")","-","_","=","+","[","{","]","}","\\","|",";",":",
            "\'","\"",",","<",".",">","/","?","※","☆","♡","\b","\t","\n","\c",
            "\r"," "
)

config_ip_certfd = open("config/ip_cert.txt","r")
config_ip_devfd = open("config/ip_dev.txt","r")
config_ip_domesticfd = open("config/ip_domestic.txt","r")
config_ip_foreignfd = open("config/ip_foreign.txt","r")
config_ip_intrafd = open("config/ip_intra.txt","r")
config_ip_suspiciousfd = open("config/ip_suspicious.txt","r")
config_port_certfd = open("config/port_cert.txt","r")
config_port_common_usefd = open("config/port_common_use.txt","r")
config_port_devfd = open("config/port_dev.txt","r")
config_port_exploitablefd = open("config/port_exploitable.txt","r")
config_port_issuefd = open("config/port_issue.txt","r")

ip_cert = config_ip_certfd.read().splitlines()
ip_dev = config_ip_devfd.read().splitlines()
ip_domestic = config_ip_domesticfd.read().splitlines()
ip_foreign = config_ip_foreignfd.read().splitlines()
ip_intra = config_ip_intrafd.read().splitlines()
ip_suspicious = config_ip_suspiciousfd.read().splitlines()
port_cert = config_port_certfd.read().splitlines()
port_common_use = config_port_common_usefd.read().splitlines()
port_dev = config_port_devfd.read().splitlines()
port_exploitable = config_port_exploitablefd.read().splitlines()
port_issue = config_port_issuefd.read().splitlines()





def one_hot_ecoding(str):

    x = []
    for i,char in enumerate(str):
        try:
            num = charset.index(char)
        except:
            print("Unhandeld Character :",char)
            sys.exit()
            
        vector = [0] * len(charset)
        vector[num] = 1
        x.append(vector)
    
    return x


def file_to_hotencoding(path,label_number):

    X = []
    Y = []

    lines = open(path,"r").readlines()
    for line in lines:
        line = line.lower()
        x = one_hot_ecoding(line)
        X.append(np.asarray(x).astype(np.float32))
        Y.append(label_number)
        
    return X,Y
    
    
    
X,Y = file_to_hotencoding("config/pay_load_threat.txt",1)
legX, legY = file_to_hotencoding("config/pay_load_legitimate.txt",0)
X.extend(legX)
Y.extend(legY)
X = pad_sequences(X,dtype='object',maxlen=sequence_maxlength,value= [0]*len(charset),padding="post")
X = np.asarray(X).astype(np.float32)
Y = np.asarray(Y).astype(np.float32)
model = keras.Sequential()
model.add(keras.Input((None,77)))
model.add(layers.LSTM(50))
model.add(layers.Dense(1, activation='sigmoid'))
model.compile(optimizer="Adam", loss="mse")
model.fit(X, Y,epochs=100, batch_size=50, verbose=1)



def ip_assessment(ip):
    
    if ip in ip_cert :
        return -100
    elif ip in ip_dev :
        return -50
    elif ip in ip_intra :
        return -10
    elif ip in ip_domestic :
        return 0
    elif ip in ip_foreign :
        return 20
    elif ip in ip_suspicious :
        return 90
    else:
        return 20
        


    
def port_assessment(port):

    
    if port in port_cert:
        return -100
    elif port in port_dev:
        return -50
    elif port in port_common_use:
        return 0
    elif port in port_exploitable:
        return 10
    elif port in port_issue:
        return 50
    else:
        return 1

        
    
def payload_assessment(payload):
    payloads = []
    payload = payload.lower()
    payload = one_hot_ecoding(payload)
    payloads.append(payload)
    payloads = pad_sequences(payloads,dtype='object',maxlen=sequence_maxlength,value= [0]*len(charset),padding="post")
    predictions = model.predict(np.asarray(payloads).astype(np.float32))
    return int(predictions[0][0]*100)
    


if __name__ == "__main__":

    access_log = open(sys.argv[1],"r").read().splitlines()
    found = re.findall(r"(.*?) - - \[.*\] \".*? (.*) (HTTPS?).*?\" \d\d\d \d.*",access_log[0])[0]
    port = 0
    if found[2]=="HTTP":
        port = 80
    else:
        port = 443
    res = {"ip_risk":ip_assessment(found[0]),"payload_risk":payload_assessment(found[1]),"port_risk":port_assessment(port)}
    dectree = DecisionTree()
    dectree.fit_with_jsonfile( "preprocessed_dataset.txt",10)
    if dectree.predict( res ) == 0:
        print("정상적인 웹 접근입니다.")
    elif dectree.predict( res ) == 1:
        print("의심되는 트래픽입니다. 상세분석을 요청합니다.")
    elif dectree.predict( res ) == 2:
        print("공격적인 웹 접근입니다.")
    











    