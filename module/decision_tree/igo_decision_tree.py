import math
import random
import queue
import sys
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.Qt import *
import numpy as np
import itertools
from ast import literal_eval




class Node:
    def __init__(self):
        self.sample_data = None
        self.impurity = None
        self.feature = None
        self.threshold = None 
        self.involvedclass = None
        self.leftsubset = []
        self.rightsubset = []
        self.left = self.right = None
        self.color = None
        self.depth = None

def gini(dataset,labels,attribute, classes_restriction):
    
    subset = []
    res = 0
    
    for i in range(0,classes_restriction):
        subset.append([])        

    for d,l in zip(dataset,labels):
        subset[l].append(d)

    for i in range(0,classes_restriction):
        res +=  ( 1- len(subset[i]) / len(dataset)) * (len(subset[i]) / len(dataset))
    return res,subset



def partition(dataset, labels,attribute , threshold):
    

    right = []
    left = []
    rightlab = []
    leftlab = []
    minimum = math.inf
    maximum = -math.inf


    for d,l in zip(dataset,labels):
        if(d[attribute] < threshold ):
            if maximum < d[attribute]:
                maximum = d[attribute]
            left.append(d)
            leftlab.append(l)
        else :
            if minimum > d[attribute]:
                minimum = d[attribute]
            right.append(d)
            rightlab.append(l)


    return right, left,rightlab,leftlab, maximum, minimum



def preorderImpurity(dectree,dataset, labels,node,depth):

    lowestright = lowestleft = lowestleftlab = lowestrightlab= []
    smallest_impurity = math.inf
    highest_impurity = -math.inf
    meet_feature = 0
    meet_threshold = 0
    lowestleftmax = 0
    lowestrightmin = 0
    max_count_features = dectree.max_count_features
    max_count_classes = dectree.max_count_classes
    max_depth = dectree.max_depth
    feature_names = dectree.feature_names

    for data in dataset:
        impurity = None
        for i in range(0,max_count_features):
            right, left,rightlab,leftlab,leftmax,rightmin = partition(dataset,labels,i,data[i])


            if len(dataset) == 0 :
                impurity = 0
                leftsubsets = []
                for i in range(max_count_classes):
                    leftsubsets.append([])
                rightsubsets = []
                for i in range(max_count_classes):
                    rightsubsets.append([])
            elif len(left) == 0 and len(right) != 0 :
                leftgini = 0
                leftsubsets = []
                for i in range(max_count_classes):
                    leftsubsets.append([])
                rightgini, rightsubsets = gini(right,rightlab,i,max_count_classes)
                impurity = rightgini
            elif len(left) != 0 and len(right) == 0:
                rightgini = 0
                rightsubsets = []
                for i in range(max_count_classes):
                    rightsubsets.append([])
                leftgini,leftsubsets = gini(left,leftlab,i,max_count_classes)
                impurity = rightgini
            else:
                leftgini,leftsubsets = gini(left,leftlab,i,max_count_classes)
                rightgini, rightsubsets = gini(right,rightlab,i,max_count_classes)
                impurity = len(left)/len(dataset) * leftgini + len(right)/len(dataset) *rightgini 
        
            if(smallest_impurity > impurity):
                smallest_impurity  = impurity
                meet_feature = i
                meet_threshold = data[i]
                lowestleft = left
                lowestright = right
                lowestleftlab = leftlab
                lowestrightlab = rightlab
                lowestleftsubsets = leftsubsets
                lowestrightsubsets = rightsubsets
                lowestleftmax = leftmax
                lowestrightmin = rightmin

    if smallest_impurity == math.inf or smallest_impurity == 0 or depth >= max_depth:
        standalone_impurity = 0
        for i in range(max_count_classes):
            len_subsets = len(lowestleftsubsets[i])+len(lowestrightsubsets[i])
            standalone_impurity += ( 1- len_subsets / len(dataset)) * len_subsets / len(dataset)


        if standalone_impurity != 0 and depth < max_depth:
            node.impurity = smallest_impurity
            node.feature = meet_feature
            node.threshold = (lowestleftmax+lowestrightmin)/2
            node.involvedclass = -1
            node.leftsubset = lowestleftsubsets
            node.rightsubset = lowestrightsubsets
            
            color = [random.randrange(50,200),random.randrange(50,200),random.randrange(50,200)]

            leftn = Node()
            node.left = leftn
            leftn.color = color
            leftn.depth = depth+1
            leftn.involvedclass = lowestleftlab[0]
            leftn.leftsubset = lowestleftsubsets
            leftn.rightsubset = [ [] for i in range(max_count_classes)]
            

            rightn = Node()
            node.right = rightn
            rightn.color = color
            rightn.depth = depth+1
            rightn.involvedclass = lowestrightlab[0]
            rightn.leftsubset = [ [] for i in range(max_count_classes)]
            rightn.rightsubset = lowestrightsubsets

        else:
            node.impurity = smallest_impurity
            node.feature = meet_feature
            node.threshold = (lowestleftmax+lowestrightmin)/2
            node.involvedclass = labels[0]
            node.leftsubset = lowestleftsubsets
            node.rightsubset = lowestrightsubsets
            return 

    node.impurity = smallest_impurity
    node.feature = meet_feature
    node.threshold = (lowestleftmax+lowestrightmin)/2
    node.involvedclass = -1
    node.leftsubset = lowestleftsubsets
    node.rightsubset = lowestrightsubsets


    if len(lowestleft) == 0 and len(lowestright) == 0:
        return 


    elif len(lowestleft) != 0 and len(lowestright) == 0:

        leftn = Node()
        node.left = leftn
        leftn.color = [255,0,0]
        leftn.depth = depth+1
        leftn.involvedclass = -2
        leftn.leftsubset = lowestleftsubsets
        leftn.rightsubset = [ [] for i in range(max_count_classes)]
        leftn.sample_data = dataset[0]
        return


    elif len(lowestleft) == 0 and len(lowestright) != 0:

        rightn = Node()
        node.right = rightn
        rightn.color = [255,0,0]
        rightn.depth = depth+1
        rightn.involvedclass = -2
        rightn.leftsubset = [ [] for i in range(max_count_classes)]
        rightn.rightsubset = lowestrightsubsets
        rightn.sample_data = dataset[0]
        return 

    else:
        color = [random.randrange(50,200),random.randrange(50,200),random.randrange(50,200)]
        leftn = Node()
        node.left = leftn
        leftn.color = color
        leftn.depth = depth+1
        preorderImpurity(dectree,lowestleft, lowestleftlab,leftn,depth+1)
        rightn = Node()    
        node.right = rightn
        rightn.color = color
        rightn.depth = depth+1
        preorderImpurity(dectree,lowestright , lowestrightlab,rightn,depth+1)





class AppInst(QWidget):
    
    def __init__(self,dectree):
        super().__init__()
        

        hbox = QHBoxLayout()
        vbox = QVBoxLayout(self)
        groupBox = QGroupBox()    
        screen = QDesktopWidget().screenGeometry()        

        max_count_features = dectree.max_count_features
        max_count_classes = dectree.max_count_classes
        max_depth = dectree.max_depth
        feature_names = dectree.feature_names

        q = queue.Queue()
        q.put(dectree.dectree)
        prevdepth = 1
        n = None
        while not q.empty():
            
            if n !=None:
                prevdepth = n.depth
            n = q.get()

            if prevdepth < n.depth :                
                vbox.addLayout(hbox)
                hbox = QHBoxLayout()

                

            if n.involvedclass == -1:
                b = QPushButton("feature : "+feature_names[n.feature]+"\ntheshold : "+str(n.threshold)+"\nimpurity : "+str(n.impurity))
                b.setMaximumWidth(300)
                b.setMinimumHeight(100)
                tooltip_string = "left"
                for i,leftss in enumerate(n.leftsubset):
                    tooltip_string += "\n class "+str(i)+":"+str(len(leftss))
            
                tooltip_string += "\nright"
                for i,rightss in enumerate(n.rightsubset):
                    tooltip_string += "\n class "+str(i)+":"+str(len(rightss))
                b.setToolTip(tooltip_string)
                b.setStyleSheet("background-color:rgb("+str(n.color[0])+","+str(n.color[1])+","+str(n.color[2])+");")
                hbox.addWidget(b)



            elif n.involvedclass == -2 :
                string = "Bad leaf !!!"
                i = 0
                for leftss,rightss in itertools.zip_longest(n.leftsubset,n.rightsubset):
                    string += "\n class "+str(i)+":"+str(len(leftss)+len(rightss))
                    i+=1
                b=QPushButton(string)
                b.setMaximumWidth(300)
                b.setMinimumHeight(100)
                b.setStyleSheet("background-color:rgb("+str(n.color[0])+","+str(n.color[1])+","+str(n.color[2])+");")

                tooltip_string = "sample\n"
                for i in range(0,len(feature_names)):
                    tooltip_string += " "+feature_names[i]+" : "+str(n.sample_data[i])+"\n"

                b.setToolTip(tooltip_string)
                hbox.addWidget(b)
                
                
            else :
                b= QPushButton("It is leaf as : ["+str(n.involvedclass)+"] - ")
                b.setMaximumWidth(300)
                b.setMinimumHeight(100)
                b.setStyleSheet("background-color:rgb("+str(n.color[0])+","+str(n.color[1])+","+str(n.color[2])+");")

                i = 0
                tooltip_string = "leaf"
                for leftss,rightss in itertools.zip_longest(n.leftsubset,n.rightsubset):
                    tooltip_string += "\n class "+str(i)+":"+str(len(leftss)+len(rightss))
                    i+=1
                b.setToolTip(tooltip_string)
                hbox.addWidget(b)

            if n.left != None:
                q.put(n.left)
            if n.right != None:
                q.put(n.right)


        vbox.addLayout(hbox)


        groupBox.setLayout(vbox)
        scroll = QScrollArea(self)
        scroll.setWidgetResizable(False)
        scroll.setWidget(groupBox)


        self.setGeometry(0,0,screen.width(),screen.height())
        scroll.setGeometry(0,0,screen.width()-10,screen.height()-50)
        self.show()

    def center(self):
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())    


class DecisionTree:

    def __init__(self):
        self.dectree = None
        self.max_count_classes = None
        self.max_count_features = None
        self.feature_names = []
        self.max_depth = 0
        
    def fit(self,dataset,labels,feature_names):
        self.dectree = Node()
        self.dectree.color = [random.randrange(50,256),random.randrange(50,256),random.randrange(50,256)]
        self.dectree.depth=1
        
        self.feature_names = feature_names
        self.max_count_features = len(self.feature_names)
        self.max_count_classes = len(labelset)
        self.max_depth = max_depth
              
         
        preorderImpurity(self,dataset, labels,self.dectree,1)


    def fit_with_jsonfile(self,filename,max_depth):

        labels = []
        dataset = []
        labelset = set()

        self.dectree = Node()
        self.dectree.color = [random.randrange(50,256),random.randrange(50,256),random.randrange(50,256)]
        self.dectree.depth=1
        
        lines = open(filename,"r",encoding="utf-8")

        sample = literal_eval(lines.readline())
        inputdata = sample

        for feature in sample:
            if feature.lower() == "@label" or feature.lower() == "@description":
                continue
            self.feature_names.append(feature)

        lines.close()
        lines = open(filename,"r",encoding="utf-8")

        count=0
        for line in lines:
            if line == "\n":
                break
            dic = literal_eval(line)
            labels.append(int(dic["@label"]))
            labelset.add(int(dic["@label"]))
            dataset.append([])
            for f in self.feature_names:
                dataset[count].append(int(dic[f]))
            count += 1

        self.max_count_features = len(self.feature_names)
        self.max_count_classes = len(labelset)
        self.max_depth = max_depth

        preorderImpurity(self,dataset, labels,self.dectree,1)
    def render(self):
        app = QApplication(sys.argv)
        ex = AppInst(self)
        sys.exit(app.exec_())
    def predict(self,input_data):
        node = self.dectree
        feature_names = self.feature_names
        chain = []
        while node.involvedclass == -1:
            chain.append({feature_names[node.feature]:input_data[feature_names[node.feature]]})
            if input_data[feature_names[node.feature]] < node.threshold :
                node = node.left
            else :
                node = node.right
        if node.involvedclass == -2:
            
            max_size = -math.inf
            involvedclass = None
            count = 0
            for leftss,rightss in itertools.zip_longest(node.leftsubset,node.rightsubset):
                size = len(leftss)+len(rightss)
                if size > max_size:
                    max_size = size
                    involvedclass = count
                count += 1
            return involvedclass, chain
        else:
            return node.involvedclass, chain